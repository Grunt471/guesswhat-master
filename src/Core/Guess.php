<?php

namespace App\Core;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{

  /**
   * @var $cards array a array of Cards
   */


  /**
   * @var $selectedCard Card This is the card to be guessed by the player
   */
  private $selectedCard;

    /**
     * Guess constructor.
     * @param Card $selectedCard
     */


    private $help;


    private $essai;

    private $nbCards;

    /**
     * Guess constructor.
     * @param Card $selectedCard
     * @param $help
     * @param $essai
     * @param $nbCards
     * @param $cards
     */

    public function __construct(Card $selectedCard, $help, $essai, $nbCards, $cards)
    {
        $this->selectedCard = $selectedCard;
        $this->help = $help;
        $this->essai = $essai;
        $this->nbCards = $nbCards;
        $this->cards = $cards;
    }

    public function createDeck(){
        $cards = [];
        foreach( Card::$tabColor as $x ) {
            foreach( Card::$tabNum as $y){
                $cardCreate= new Card($y, $x);
                array_push($cards, $cardCreate);
                }
            }
        print_r($cards);
    }

    public function getRandCard($cards){
        $selectedCard=$cards[rand(0, count($cards)-1)];
        return $selectedCard;
    }

    public function getHelp(): string
    {
        return $this->help;
    }

    public function getNbCarteDeck(): int
    {
        $long = count($this->cards);
        return $long;
    }


}

