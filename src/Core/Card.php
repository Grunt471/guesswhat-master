<?php

namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    const tabColor = [
        'Coeur' => 4,
        'Carreau' => 3,
        'Trèfle' => 2,
        'Pique' => 1,
    ];

    const tabNum = [
        'As' => 13,
        'Roi' => 12,
        'Reine' => 11,
        'Valet' => 10,
        'Dix' => 9,
        'Neuf' => 8,
        'Huit' => 7,
        'Sept' => 6,
        'Six' => 5,
        'Cinq' => 4,
        'Quatre' => 3,
        'Trois' => 2,
        'Deux' => 1,
    ];

    /**
     * @var $name string nom de la carte, comme par exemples 'As' 'Deux' 'Reine'
     */
    private $name;

    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $o1 Card
     * @param $o2 Card
     * @return int
     * <ul>
     *  <li> zéro si $o1 et $o2 sont considérés comme égaux </li>
     *  <li> -1 si $o1 est considéré inférieur à $o2</li>
     * <li> +1 si $o1 est considéré supérieur à $o2</li>
     * </ul>
     *
     */
    public static function cmp(Card $o1, Card $o2): int
    {


        $o1Name = $o1->name;
        $o2Name = $o2->name;

        $o1Color = $o1->color;
        $o2Color = $o2->color;

        $y = 0;
        $x = (self::tabNum[$o1Name] == self::tabNum[$o2Name]) ? 0 : +1;
        if ($x == +1) {
            if (self::tabNum[$o1Name] > self::tabNum[$o2Name]) {
                $y = +1;
                }
            elseif (self::tabNum[$o1Name] < self::tabNum[$o2Name]) {
                $y = -1;
                }
            }
        elseif ($x == 0){
            if (self::tabColor[$o1Color] > self::tabColor[$o2Color]) {
                $y = +1;
            } elseif (self::tabColor[$o1Color] < self::tabColor[$o2Color]) {
                $y = -1;
            }
            }
            return $y;
            }

    public function __toString()
    {
        $name = $this->getName();
        $color = $this->getColor();
        $x = "Cette carte est un(e) $name de $color";
        return $x;
    }

}

?>

