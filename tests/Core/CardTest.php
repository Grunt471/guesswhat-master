<?php
namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;


class CardTest extends TestCase
{
  public function testName()
  {
    $card = new Card('As', 'Trèfle');
    $this->assertEquals('As', $card->getName());
    $card = new Card('Deux', 'Trèfle');
    $this->assertEquals('Deux', $card->getName());
  }


  public function testColor()
  {
      $card = new Card('As', 'Trèfle');
      $this->assertEquals('Trèfle', $card->getColor());
      $card = new Card('As', 'Coeur');
      $this->assertEquals('Coeur', $card->getColor());
  }

  public function testCmp()
  {
      $o1 = new Card('As', 'Trèfle');
      $o2 = new Card('As', 'Trèfle');
      $this->assertEquals(0, Card::cmp($o1,$o2));
  }

    public function testCmpMoins1()
    {
        $o1 = new Card('Trois', 'Coeur');
        $o2 = new Card('As', 'Pique');
        $this->assertEquals(-1, Card::cmp($o1,$o2));

        $o1 = new Card('As', 'Pique');
        $o2 = new Card('As', 'Coeur');
        $this->assertEquals(-1, Card::cmp($o1,$o2));

    }

    public function testCmpPlus1()
    {
        $o1 = new Card('Roi', 'Coeur');
        $o2 = new Card('Valet', 'Pique');
        $this->assertEquals(+1, Card::cmp($o1,$o2));

        $o1 = new Card('Trois', 'Coeur');
        $o2 = new Card('Trois', 'Pique');
        $this->assertEquals(+1, Card::cmp($o1,$o2));

    }


  public function testToString()
  {
      $card = new Card('As', 'Trèfle');
      $this->assertEquals('Cette carte est un(e) As de Trèfle',$card->__toString());
  }

}
